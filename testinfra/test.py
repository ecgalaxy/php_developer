def test_commands(host):
  cmd = host.run('git --version')
  assert cmd.rc == 0

  cmd = host.run('module load php81; php --version')
  assert cmd.rc == 0

  cmd = host.run('code --version')
  assert cmd.rc == 0
