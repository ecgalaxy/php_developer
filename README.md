# ECGALAXY PHP developer playbook

An Ansible playbook covering Linux desktop environments and installing the base tools allowing PHP development.

## Requirements

[Ansible](https://www.ansible.com/) is required, see [Installing Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

On Amazon Linux 2 and AWS WorkSpaces, Ansible can be quickly installed with `amazon-linux-extras`:

`sudo amazon-linux-extras install ansible2`

Alternatively, you can create a virtual environment and install Ansible within it:

`python3 -m venv venv && source venv/bin/activate && pip install --upgrade pip && pip install ansible`

Ansible will then be available in the current shell, and the virtual environment can be deleted after execution of the playbook:

`deactivate && rm -rf venv`

## Usage

First, clone this repository and open a terminal in its directory.

Then 'install' the required roles:

`ansible-galaxy install -r requirements.yml --force`

The above command imports the Ansible roles source code from their repositories, which are listed in the `requirements.yml` file.

Finally, run the playbook against your inventory.

The below command will execute the playbook locally:

`ansible-playbook --connection=local -i localhost, playbook.yml --ask-become-pass`

The `become` password corresponds to your current user password, which needs `sudo` permissions.

## Optional

You can remove 'roles' you do not need by editing the `playbook.yml` file.

See [Working with playbooks](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html) for more information on how to use Ansible playbooks.
